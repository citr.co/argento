<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Argento
 */

?>
<div class="section-ponto-de-partida">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <header class="ponto-de-partida-header">
          <h1 class="ponto-de-partida-titulo">Ponto de Partida</h1>
          <p>Com base nas pesquisas sobre escolas eficazes e na larga experiência do grupo de formadores/gestores que atuam na empresa, percebe-se que a melhoria significativa das aprendizagens dos alunos nas diferentes etapas da escolaridade está relacionada às escolas que apresentam</p>
        </header>
      </div>
      <div class="col-md-10 col-md-offset-1">
        <div class="ponto-de-partida-lista">
          <div class="ponto-de-partida-item esquerda">
            <i class="icon-porque flaticon-people"></i>
            <p>Professores e coordenadores pedagógicos que protagonizam os espaços de formação continuada.</p>
          </div>
          <div class="clear"></div>
          <div class="ponto-de-partida-item direita">
            <i class="icon-porque flaticon-clareza-de-objetivos"></i>
            <p>Educadores com clareza dos objetivos comuns e que saibam estabelecer prioridades no trabalho por competências.</p>
          </div>
          <div class="clear"></div>
          <div class="ponto-de-partida-item esquerda">
            <i class="icon-porque flaticon-pessoas-envolvidas"></i>
            <p>Professores que saibam envolver os pais e alunos na proposta pedagógica e no processo de avaliação do conhecimento.</p>
          </div>
          <div class="clear"></div>
          <div class="ponto-de-partida-item direita">
            <i class="icon-porque flaticon-consolidado"></i>
            <p>Projetos pedagógicos consolidados.</p>
          </div>
          <div class="clear"></div>
          <div class="ponto-de-partida-item esquerda">
            <i class="icon-porque flaticon-comunidade"></i>
            <p>Equipe gestora que favorece a participação da comunidade escolar na tomada de decisões.</p>
          </div>
          <div class="clear"></div>
          <div class="ponto-de-partida-item direita">
            <i class="icon-porque flaticon-diretor"></i>
            <p>Liderança do diretor</p>
          </div>
          <div class="clear"></div>
          <i style="height: 663px;" class="ponto-de-partida-linha"></i>
        </div>
      </div>
    </div>
  </div><!-- .container -->
</div><!-- .section-quem-somos -->