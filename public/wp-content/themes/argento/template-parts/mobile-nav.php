<div class="mobile-menu">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <button class="mobile-close" type="button" name="button">fechar</button>
        <div class="clearfix"></div>
          <nav>
              <ul>
                  <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><b>Página</b>Inicial</a></li>
                  <li><a href="/cursos"><b>Nossos</b>Cursos</a></li>
                  <li><a href="/quem-somos"><b>Quem</b>Somos</a></li>
                  <li><a href="/contato"><b>Fale</b>Conosco</a></li>
                  <li class="social-links"><a href="#"></a></li>
                  <li class="social-links"><a href="#"></a></li>
                  <li class="social-links"><a href="#"></a></li>
              </ul>
          </nav>
      </div>
    </div>
  </div>
</div>
