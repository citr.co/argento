## Argento WordPress Theme
This theme is developed exclusively to company Argento using underscores, gulp and sass and npm as package manager

### Installation
Argento requires Node.js, to get theme running properly you should first run:
```sh
$ npm install
```
This theme also depends on some wordpress plugins to work properly:
* advanced-custom-fields
* contact-form-7

### Gulp tasks
to compile sass you can use one of two gulp tasks:
* `gulp styles`
* `gulp styles:watch`