var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', function() {
    gulp.src('sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css/'))
});

//Watch task
gulp.task('styles:watch',function() {
    gulp.watch('sass/**/*.scss',['styles']);
    gulp.watch('sass/base/**/*.scss',['styles']);
});


// Handle the error
function errorHandler (error) {
  console.log(error.toString());
  this.emit('end');
}
