<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Argento
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer hidden-xs hidden-sm" role="contentinfo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <ul>
                                <li class="footer-menu-title">Argento</li>
                                <li><a href="/quem-somos">A Argento</a></li>
                                <li><a href="/manifesto">Manifesto Argento</a></li>
                                <li><a href="/cursos">Cursos</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <li class="footer-menu-title">Redes Sociais</li>
                                <li><a target="_blank" href="https://www.facebook.com/argentoprofissional/">Facebook</a></li>
                                <li><a target="_blank" href="https://www.instagram.com/argento_desenvolvimento/">Instagram</a></li>
                                <li><a target="_blank" href="https://pt.linkedin.com/company/argento-desenvolvimento-profissional">Linkedin</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <li class="footer-menu-title">Links úteis</li>
                                <li><a href="/contato">Fale Conosco</a></li>
                                <li><a href="/contato">Trabalhe Conosco</a></li>
                                <li><a href="/mapa-do-site">Mapa do Site</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-md-offset-1">
                            <ul>
                                <li class="footer-menu-title">Mídia</li>
                                <li><a href="/artigos">Artigos</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <li class="footer-menu-title">Contato</li>
                                <li><a href="mailto:contato@argento.pro.br">contato@argento.pro.br</a></li>
                                <li><a href="tel:(11) 4329-7417">(11) 4329-7417</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
	<footer class="last-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
									<div class="contact hidden-md hidden-lg">
										<ul>
											<li><a href="#"></a></li>
											<li><a href="#"></a></li>
											<li><a href="#"></a></li>
										</ul>
										<a href="tel:(11) 4329-7417">11 4329-7417</a>
									</div>
									<div class="copyright">
										<a href="http://www.citr.co" target="_blank"><img src="<?php echo get_bloginfo('template_url') ?>/img/logo-citr-co.png" alt="Cítr.co Logomark" title="Design &amp; Development: citr.co"></a>
	                                    <p>ARGENTO DESENVOLVIMENTO PROFISSIONAL 2015. Todos os direitos reservados.</p>
									</div>
                </div>
            </div>
        </div>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/argento.custom.js"></script>
</body>
</html>
