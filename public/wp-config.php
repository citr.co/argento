<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         ':^=bgh?D+c[KvUd+|^kcn>&DIrOC/G]=1P$Gly=!QfJ|lRKQp_BzjZ-|R$yXns,.');
define('SECURE_AUTH_KEY',  '6^T>4=tCg/Ji[HS#SfMt|x|B-U>1WT|Yu,zO2Hp83{$2Eb<ss 6 -]O]2[UAa=iQ');
define('LOGGED_IN_KEY',    'b;YzZ,:x@g;&|j7F0]O-/-@i=aemb&gYH+}u|~w]Z;RM{?1|3gN}zrLEuhK}2+AV');
define('NONCE_KEY',        'r%7_D+$0p6s!!4d(t{Tv9x,;8N9m]BXp(STo?w]1[/+h eg+zcOugz2,8s^t.8b9');
define('AUTH_SALT',        'M.[`[kllcFYr+mF7t o|KX:dG0=baeIbiV{!X)m+ymTMfJCG)>PM+O|4! oE[2!u');
define('SECURE_AUTH_SALT', 'j~+|3Zt|I+B^* R_(lSf8^l[~A0u-|frFowU{|;dR.-45Dy`Gq%|,z2Zx?7$x++n');
define('LOGGED_IN_SALT',   'mnwB1a+@pYT?)|sz/l@Xm?2&lK9JKXm+2|-Kt~HpT35q:]Fr+DxI{->v = &W#.x');
define('NONCE_SALT',       'cU+^LWaZnMY+rw=uc4,!5#FVy`Je`2O;%Ildv}1|rqIG]~h/-8-!_XY#zE?8vi@k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'pt_BR');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Disable Automatic Updates Completely */
define( 'AUTOMATIC_UPDATER_DISABLED', False );

/** Define AUTOMATIC Updates for Components. */
define( 'WP_AUTO_UPDATE_CORE', True );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/wp/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
